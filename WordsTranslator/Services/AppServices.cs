﻿using System;
using Microsoft.Extensions.DependencyInjection;
using WordsTranslator.ViewModels;

namespace WordsTranslator.Services
{
    public class AppServices
    {
        private static readonly object _instanceLock = new object();
        private static AppServices _instance;

        public AppServices()
        {
            var services = new ServiceCollection();

            // view-models
            services.AddTransient<TranslateViewModel>();

            // stateful services
            services.AddScoped<ITranslationService, TranslationService>();

            ServiceProvider = services.BuildServiceProvider();
        }

        public IServiceProvider ServiceProvider { get; }

        public static AppServices I
        {
            get
            {
                lock (_instanceLock)
                {
                    return _instance ??= new AppServices();
                }
            }
        }

    }
}
