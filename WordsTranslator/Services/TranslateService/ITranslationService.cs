﻿using System;
using System.Collections.Generic;
using WordsTranslator.Services.Models;

namespace WordsTranslator.Services
{
    public interface ITranslationService
    {
        List<Record> Records { get; set; }
    }
}
