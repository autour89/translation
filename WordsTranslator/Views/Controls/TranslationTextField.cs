﻿using System;
using CoreGraphics;
using UIKit;

namespace WordsTranslator.Views.Controls
{
    public class TranslationTextField : UITextField
    {
      
        public TranslationTextField()
        {
            InitView();
        }

        public TranslationTextField(CGRect frame) : base(frame)
        {
            InitView();
        }

        public TranslationTextField(IntPtr handle) : base(handle)
        {

        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();

            InitView();
        }

        protected void InitView()
        {
            TranslatesAutoresizingMaskIntoConstraints = false;
        }

        /// <summary>
        /// Rotates the view.
        /// </summary>
        public void RotateView()
        {
            Animate(.6f, () =>
            {
                var transform = CGAffineTransform.Rotate(Transform, (float)Math.PI);
                Transform = transform;
                transform = CGAffineTransform.Rotate(Transform, (float)Math.PI);
                Transform = transform;
            });
        }

    }
}
