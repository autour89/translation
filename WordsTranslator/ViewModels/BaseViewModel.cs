﻿using System;
using System.Reactive;
using System.Threading.Tasks;
using ReactiveUI;

namespace WordsTranslator.ViewModels
{
    public abstract class BaseViewModel : ReactiveObject
    {
        private ObservableAsPropertyHelper<bool> _isLoading;

        protected abstract Task LoadAsync();

        public bool IsLoading => _isLoading.Value;

        public ReactiveCommand<Unit, Unit> ReloadCommand { get; private set; }

        public BaseViewModel()
        {
            ReloadCommand = ReactiveCommand.CreateFromTask(LoadAsync);
            ReloadCommand.IsExecuting.ToProperty(this, x => x.IsLoading, out _isLoading);
        }

    }
}
