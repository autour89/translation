﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using WordsTranslator.Services.Models;

namespace WordsTranslator.Services
{
    public class TranslationService : ITranslationService
    {
        private const string FilePath = "Services/Resources/eKidzDictionary";

        public List<Record> Records { get; set; }

        public TranslationService()
        {
            RestoreDictionary();
        }

        private Task RestoreDictionary()
        {
            var translationRecords = File.ReadAllText(FilePath + ".json");

            var options = new JsonSerializerOptions
            {
                Converters = { new JsonStringEnumConverter(JsonNamingPolicy.CamelCase) }
            };

            Records = JsonSerializer.Deserialize<Translations>(translationRecords, options).Record;

            return Task.CompletedTask;
        }

    }
}
