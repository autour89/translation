﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace WordsTranslator.Services.Models
{

    public class Translations
    {
        [JsonPropertyName("record")]
        public List<Record> Record { get; set; }
    }

    public class Record
    {
        [JsonPropertyName("word")]
        public string Word { get; set; }

        [JsonPropertyName("culture")]
        public string Culture { get; set; }

        [JsonPropertyName("link")]
        public List<Link> Links { get; set; }
    }

    public class Link
    {
        [JsonPropertyName("word")]
        public string Word { get; set; }

        [JsonPropertyName("culture")]
        public string Culture { get; set; }
    }

}
