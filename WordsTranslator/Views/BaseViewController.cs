﻿using System;
using Foundation;
using UIKit;

namespace WordsTranslator.Views
{
    public abstract class BaseViewController : UIViewController
    {
        private UIView _activeview;                                  // Controller that activated the keyboard
        private nfloat _scroll_amount = 0.0f;                        // amount to scroll 
        private nfloat _bottom = 0.0f;                               // bottom point
        private float _offset = 0.0f;                                // extra offset
        private bool _moveViewUp = false;                            // which direction are we moving

        protected abstract UIView DefaultActiveview { get; }                

        protected BaseViewController(string nibName, NSBundle bundle) : base(nibName, bundle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Keyboard popup
            NSNotificationCenter.DefaultCenter.AddObserver
            (UIKeyboard.DidShowNotification, KeyBoardUpNotification);

            // Keyboard Down
            NSNotificationCenter.DefaultCenter.AddObserver
            (UIKeyboard.WillHideNotification, KeyBoardDownNotification);

            DismissKeyboardOnBackgroundTap();
        }


        protected void KeyBoardUpNotification(NSNotification notification)
        {
            // get the keyboard size
            var r = UIKeyboard.BoundsFromNotification(notification);

            // Find what opened the keyboard
            foreach (UIView view in this.View.Subviews)
            {
                if (view.IsFirstResponder)
                    _activeview = view;
            }

            if(_activeview == null)
            {
               _activeview = DefaultActiveview;
            }

            // Bottom of the controller = initial position + height + offset      
            _bottom = (_activeview.Frame.Y + _activeview.Frame.Height + _offset);

            // Calculate how far we need to scroll
            _scroll_amount = (r.Height - (View.Frame.Size.Height - _bottom));

            // Perform the scrolling
            if (_scroll_amount > 0)
            {
                _moveViewUp = true;
                ScrollTheView(_moveViewUp);
            }
            else
            {
                _moveViewUp = false;
            }
        }

        protected void KeyBoardDownNotification(NSNotification notification)
        {
            if (_moveViewUp) { ScrollTheView(false); }
        }

        private void ScrollTheView(bool move)
        {
            // scroll the view up or down
            var frame = View.Frame;

            if (move)
            {
                frame.Y -= _scroll_amount;
            }
            else
            {
                frame.Y += _scroll_amount;
                _scroll_amount = 0;
            }

            View.Frame = frame;
            UIView.CommitAnimations();
        }


        protected void DismissKeyboardOnBackgroundTap()
        {
            // Add gesture recognizer to hide keyboard 
            var tap = new UITapGestureRecognizer { CancelsTouchesInView = false };
            tap.AddTarget(() => View.EndEditing(true));
            View.AddGestureRecognizer(tap);
        }

    }
}
