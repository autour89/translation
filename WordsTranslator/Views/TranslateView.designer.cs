﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using WordsTranslator.Views.Controls;

namespace WordsTranslator.Views
{
    [Register("TranslateView")]
    partial class TranslateView
    {
        [Outlet]
        UIScrollView _scrollView { get; set; }

        [Outlet]
        UIActivityIndicatorView _activityIndicator { get; set; }

        [Outlet]
        UIView _translateFieldView { get; set; }

        [Outlet]
        TranslationTextField _translateTextField { get; set; }

        [Outlet]
        UIButton _translateButton { get; set; }

        [Outlet]
        UILabel _translation { get; set; }

        void ReleaseDesignerOutlets()
        {
            if (_scrollView != null)
            {
                _scrollView.Dispose();
                _scrollView = null;
            }

            if (_activityIndicator != null)
            {
                _activityIndicator.Dispose();
                _activityIndicator = null;
            }

            if (_translateFieldView != null)
            {
                _translateFieldView.Dispose();
                _translateFieldView = null;
            }

            if (_translateTextField != null)
            {
                _translateTextField.Dispose();
                _translateTextField = null;
            }

            if (_translateButton != null)
            {
                _translateButton.Dispose();
                _translateButton = null;
            }

            if (_translation != null)
            {
                _translation.Dispose();
                _translation = null;
            }
        }
    }
}
