﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Threading.Tasks;
using ReactiveUI;
using WordsTranslator.Services;
using WordsTranslator.Services.Models;

namespace WordsTranslator.ViewModels
{
    public class TranslateViewModel : BaseViewModel
    {
        private readonly string _noSearchResult = "No search results";
        private readonly ITranslationService _translateService;

        private string _record;
        public string Record 
        { 
            get => _record;
            set
            {
                if(_record != value && IsTranslated)
                {
                    IsTranslated = false;
                }

                this.RaiseAndSetIfChanged(ref _record, value);
            }
        }

        private string _translation;
        public string Translation
        {
            get => _translation;
            set
            {
                this.RaiseAndSetIfChanged(ref _translation, value);
            }
        }

        private bool _isTranslated;
        public bool IsTranslated
        {
            get => _isTranslated;
            set
            {
                _isTranslated = value;
                this.RaisePropertyChanged();
            }
        }

        public ReactiveCommand<Unit, Unit> TranslateCommand { get; set; }

        public TranslateViewModel(ITranslationService translateService)
        {
            _translateService = translateService;

            TranslateCommand = ReactiveCommand.CreateFromTask(TranslateAsync);
        }

        private Task TranslateAsync()
        {
            var links = _translateService.Records.Find(d => d.Word == Record)?.Links ?? new List<Link>();

            if (!links.Any())
            {
                IsTranslated = false;
                Translation = _noSearchResult;
                return Task.CompletedTask;
            }

            var translation =string.Empty;
            var index = 1;
            foreach (var link in links)
            {
                translation += $"{index}. {link.Word}{Environment.NewLine}";
            }
            Translation = translation;

            IsTranslated = true;

            return Task.CompletedTask;
        }

        protected override Task LoadAsync()
        {
            return Task.CompletedTask;
        }
    }
}
