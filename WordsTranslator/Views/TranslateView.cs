﻿using Microsoft.Extensions.DependencyInjection;
using ReactiveUI;
using UIKit;
using WordsTranslator.Services;
using WordsTranslator.ViewModels;

namespace WordsTranslator.Views
{
    public partial class TranslateView : BaseViewController, IViewFor<TranslateViewModel>
    {
        private readonly IServiceScope _scope;

        #region ViewModel Setup
        public TranslateViewModel ViewModel { get; set; }
        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = value as TranslateViewModel;
        }
        #endregion

        private bool _isTranslated;
        public bool IsTranslated
        {
            get => _isTranslated;
            set
            {
                _isTranslated = value;

                if(_isTranslated)
                {
                    _translateTextField.RotateView();
                }
            }
        }

        protected override UIView DefaultActiveview => _translateFieldView;

        public TranslateView() : base(nameof(TranslateView), null)
        {
            _scope = AppServices.I.ServiceProvider.CreateScope();

            ViewModel = _scope.ServiceProvider.GetRequiredService<TranslateViewModel>();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            Initialize();
            InitBindings();
        }

        private void Initialize()
        {
            NavigationController.NavigationBar.Translucent = false;
            NavigationItem.Title = "Translation";
            _translateTextField.Layer.BorderWidth = 1;
            _translateTextField.Layer.BorderColor = UIColor.Brown.CGColor;
        }

        private void InitBindings()
        {
            this.Bind(ViewModel, viewModel => viewModel.Record, view => view._translateTextField.Text);
            this.Bind(ViewModel, viewModel => viewModel.Translation, view => view._translation.Text);
            this.Bind(ViewModel, viewModel => viewModel.IsTranslated, view => view.IsTranslated);
            this.Bind(ViewModel, viewModel => viewModel.IsLoading, view => view._activityIndicator.Hidden, this.ValueConverterFunc, this.ValueConverterFunc);
            this.BindCommand(ViewModel, viewModel => viewModel.TranslateCommand, view => view._translateButton);
        }

        private bool ValueConverterFunc(bool value) => !value;

    }
}

